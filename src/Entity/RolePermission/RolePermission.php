<?php

namespace App\Entity\RolePermission;

use App\Entity\Permission\Permission;
use App\Entity\Role\Role;
use App\Repository\RolePermission\RolePermissionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RolePermissionRepository::class)
 */
class RolePermission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Permission::class, inversedBy="rolePermissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Permission;

    /**
     * @ORM\ManyToOne(targetEntity=Role::class, inversedBy="rolePermissions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Role;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPermission(): ?Permission
    {
        return $this->Permission;
    }

    public function setPermission(?Permission $Permission): self
    {
        $this->Permission = $Permission;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->Role;
    }

    public function setRole(?Role $Role): self
    {
        $this->Role = $Role;

        return $this;
    }
}
