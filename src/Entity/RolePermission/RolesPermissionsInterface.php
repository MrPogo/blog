<?php


namespace App\Entity\RolePermission;


use App\Entity\Permission\Permission;
use App\Entity\Role\Role;

interface RolesPermissionsInterface
{
    public function add(RolePermission $rolePermission);
    public function delete(RolePermission $rolePermission);
    public function  findByPermission(Permission $permission);
    public function  findByRole(Role $role);
    public function  findByPermissionAndRole(Permission $permission, Role $role);
}