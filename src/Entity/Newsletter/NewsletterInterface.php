<?php


namespace App\Entity\Newsletter;



interface NewsletterInterface
{
    public function add(Newsletter $newsletter);
    public function getAll(Newsletter $newsletter);
    public function findOneByEmail(string $email);
    public function delete(Newsletter $newsletter);

}