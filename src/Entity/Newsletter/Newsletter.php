<?php

namespace App\Entity\Newsletter;

use App\Entity\User\User;
use App\Repository\Newsletter\NewsletterRepository;
use Doctrine\ORM\Mapping as ORM;
use Twig\Error\RuntimeError;

/**
 * @ORM\Entity(repositoryClass=NewsletterRepository::class)
 */
class Newsletter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Is_active;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="newsletter")
     */
    private $User;


    public function __construct(
        string $email,
        ?User $User,
        bool $Is_active=true
    )
    {
        $this->email = $email;
        $this->User = $User;
        $this->Is_active = $Is_active;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }

    public function setIsActive(bool $Is_active): self
    {
        $this->Is_active = $Is_active;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }
}
