<?php


namespace App\Entity\Newsletter\Newsletter\ReadModel;


use App\Entity\User\User;

class NewsletterReadModel
{
    private $id;
    private $email;
    private $Is_active;
    private $User;

    public function __construct(
        int $id,
        string $email,
        ?User $User,
        bool $Is_active
    )
    {

        $this->id = $id;
        $this->email = $email;
        $this->User = $User;
        $this->Is_active = $Is_active;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

}