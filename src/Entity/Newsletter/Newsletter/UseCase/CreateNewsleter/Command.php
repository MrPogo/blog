<?php


namespace App\Entity\Newsletter\Newsletter\UseCase\CreateNewsleter;


use App\Entity\User\User;

class Command
{

    private $email;
    private $Is_active;
    private $User;
    private $responder;



    public function __construct(
        string $email,
        ?User $User,
        bool $Is_active
    )
    {
        $this->email = $email;
        $this->User = $User;
        $this->Is_active = $Is_active;
        $this->responder = new NullResponder();

    }


    public function getEmail(): ?string
    {
        return $this->email;
    }


    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }


    public function getUser(): ?User
    {
        return $this->User;
    }

    public function getResponder(): Responder
    {
        return $this->responder;
    }
    /**
     * @param Responder $responder
     */
    public function setResponder(Responder $responder): void
    {
        $this->responder = $responder;
    }

}