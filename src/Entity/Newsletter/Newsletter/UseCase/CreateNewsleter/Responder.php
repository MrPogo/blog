<?php


namespace App\Entity\Newsletter\Newsletter\UseCase\CreateNewsleter;



use App\Entity\Newsletter\Newsletter;

interface Responder
{
    public function CreateNewsleter(Newsletter $newsletter);
    public function emailExists();
}