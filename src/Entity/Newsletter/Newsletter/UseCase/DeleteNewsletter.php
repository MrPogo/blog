<?php


namespace App\Entity\Newsletter\Newsletter\UseCase;


use App\Adapter\Core\Transaction;
use App\Entity\Newsletter\NewsletterInterface as Newsletter;
use App\Entity\Newsletter\Newsletter\UseCase\DeleteNewsletter\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeleteNewsletter extends AbstractController
{

    private $newsletter;
    private $transaction;
    public function __construct(
    Newsletter $newsletter, Transaction $transaction
    )
    {
        $this->newsletter=$newsletter;
        $this->transaction=$transaction;
    }
    public function execute(Command $command)
    {
        $this->transaction->begin();

        $this->newsletter->delete($command->getNewsletter());

        try {
            $this->transaction->commit();
        } catch (\Throwable $e) {
            $this->transaction->rollback();
            throw $e;
        }
    }
}