<?php


namespace App\Entity\Newsletter\Newsletter\UseCase;


use App\Adapter\Core\EmailFactory;
use App\Adapter\Core\Transaction;
use App\Adapter\Newsletter\Newsletter;
use App\Entity\Newsletter\Newsletter\UseCase\CreateNewsleter\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CreateNewsleter extends AbstractController
{
    private $Newsletter;
    private $transaction;
    private $EmailFactory;

    public function __construct(Newsletter $newsletter , Transaction $transaction,EmailFactory $emailFactory,Newsletter $newsletters)
    {
        $this->Newsletters=$newsletters;
        $this->Newsletter = $newsletter;
        $this->transaction = $transaction;
        $this->EmailFactory= $emailFactory;
    }

    public function execute(Command $command)
    {

        $this->transaction->begin();

        if($this->Newsletter->findOneByEmail($command->getEmail())){
            $command->getResponder()->emailExists();
            return;
        }

        $Newsletter = new \App\Entity\Newsletter\Newsletter(
            $command->getEmail(),
            $command->getUser(),
            $command->getIsActive()


        );


        $this->Newsletter->add($Newsletter);
        try {
            $this->transaction->commit();
        } catch (\Throwable $e) {
            $this->transaction->rollback();
            throw $e;
        }

        $command->getResponder()->CreateNewsleter($Newsletter);

    }

}