<?php


namespace App\Entity\Like\Like\ReadModel;


use App\Entity\Post\Post;
use App\Entity\User\User;

class LikeReadModel
{


    private $id;
    private $User;
    private $Post;
    private $Is_active;
    private $Data_Add;

    public function __construct(
        int $id,
        int $User,
        int $Post,
        bool $Is_active,
        \DateTime $Data_Add
    )
    {

        $this->id = $id;
        $this->User = $User;
        $this->Post = $Post;
        $this->Is_active = $Is_active;
        $this->Data_Add = $Data_Add;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?int
    {
        return $this->User;
    }


    public function getPost(): ?int
    {
        return $this->Post;
    }


    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }


    public function getDataAdd(): ?\DateTimeInterface
    {
        return $this->Data_Add;
    }

}