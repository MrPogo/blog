<?php


namespace App\Entity\Like\Like\UseCase;


use App\Adapter\Core\Transaction;
use App\Adapter\Like\Like;
use App\Entity\Like\Like\UseCase\CreateLike\Command;

class CreateLike
{

    private $Like;
    private $transaction;

    public function __construct(Like $like , Transaction $transaction)
    {
        $this->transaction = $transaction;
        $this->Like=$like;
    }

    public function execute(Command $command)
    {

        $this->transaction->begin();
        /** @var \App\Entity\Like\Like $dLike */
        $dLike=$this->Like->findLikeUserInPost($command->getPost()->getId(),$command->getUser()->getId());
       dump($dLike);
        if($dLike!=null)
        {
            $this->Like->delete($dLike);
            try {
                $this->transaction->commit();
            } catch (\Throwable $e) {
                $this->transaction->rollback();
                throw $e;
            }
            return;
        }

        $Like = new \App\Entity\Like\Like(
            $command->getUser(),
            $command->getPost()
        );




        $this->Like->add($Like);
        try {
            $this->transaction->commit();
        } catch (\Throwable $e) {
            $this->transaction->rollback();
            throw $e;
        }

        $command->getResponder()->createLike();

    }
}