<?php


namespace App\Entity\Like\Like\UseCase\CreateLike;


interface Responder
{
    public function createLike();
    public function unLike();

}