<?php


namespace App\Entity\Like\Like\UseCase\CreateLike;


use App\Entity\Like\Like;
use App\Entity\Post\Post;
use App\Entity\User\User;

class Command
{
    private $User;
    private $Post;
    private $responder;

    public function __construct(
        User $User,
        Post $Post
    )
    {

        $this->User = $User;
        $this->Post = $Post;
        $this->responder = new NullResponder();

    }




    public function getUser(): ?User
    {
        return $this->User;
    }


    public function getPost(): ?Post
    {
        return $this->Post;
    }

    public function getResponder(): Responder
    {
        return $this->responder;
    }
    /**
     * @param Responder $responder
     */
    public function setResponder(Responder $responder): void
    {
        $this->responder = $responder;
    }



}