<?php

namespace App\Entity\Like;

use App\Entity\Post\Post;
use App\Entity\User\User;
use App\Repository\Like\LikeRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LikeRepository::class)
 * @ORM\Table(name="`like`")
 */
class Like
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="likes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="likes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Post;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Is_active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Data_Add;

    public function __construct(
        User $User,
        Post $Post,
        bool $Is_active=true
    )
    {
        $this->User=$User;
        $this->Post=$Post;
        $this->Data_Add=new DateTime("now");
        $this->Is_active=$Is_active;

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->Post;
    }

    public function setPost(?Post $Post): self
    {
        $this->Post = $Post;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }

    public function setIsActive(bool $Is_active): self
    {
        $this->Is_active = $Is_active;

        return $this;
    }

    public function getDataAdd(): ?\DateTimeInterface
    {
        return $this->Data_Add;
    }

    public function setDataAdd(\DateTimeInterface $Data_Add): self
    {
        $this->Data_Add = $Data_Add;

        return $this;
    }
}
