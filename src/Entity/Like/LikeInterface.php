<?php


namespace App\Entity\Like;


use App\Entity\Post\Post;
use App\Entity\User\User;

interface LikeInterface
{
    public function add(Like $like);
    public function delete(Like $like);
    public function findLikeUserInPost(int $postID,int $userID);
}