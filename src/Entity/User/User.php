<?php

namespace App\Entity\User;

use App\Entity\Comment\Comment;
use App\Entity\Like\Like;
use App\Entity\Newsletter\Newsletter;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles =[] ;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Username;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Is_active;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Token_Expire;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="user")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity=Like::class, mappedBy="User")
     */
    private $likes;

    /**
     * @ORM\OneToOne(targetEntity=Newsletter::class, mappedBy="User", cascade={"persist", "remove"})
     */
    private $newsletter;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Is_ban;

    public function __construct(
        string $email,
        string $password,
        string $Username,
        array $roles=['ROLE_USER'],
        bool $Is_active=false,
        bool $Is_ban=false
    )
    {

        $this->email = $email;
        $this->password = password_hash($password, PASSWORD_BCRYPT);
        $this->Username = $Username;
        $this->roles=$roles;
        $this->Token =md5(uniqid(time()));
        $this->Token_Expire = new DateTime('+15 minutes');
        $this->Is_active = $Is_active;
        $this->comments = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->Is_ban =$Is_ban;
    }

    public function active(
        bool $Is_active,
        ?string $Token,
        ?\DateTime $Token_Expire
    )
    {
        $this->Is_active = $Is_active;
        $this->Token = $Token;
        $this->Token_Expire = $Token_Expire;
    }

    public function TokenExpire(
        ?string $Token,
        ?\DateTime $Token_Expire
    ){
        $this->Token = $Token;
        $this->Token_Expire = $Token_Expire;
    }

    public function PasswordChange(
        string $password,
        ?string $Token,
        ?\DateTime $Token_Expire
    ){
        $this->password=password_hash($password, PASSWORD_BCRYPT);
        $this->Token=$Token;
        $this->Token_Expire=$Token_Expire;
    }

    public function ban(
        bool $Is_ban
    ){
      $this->Is_ban=$Is_ban;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->Username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER

        return (array) $roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(string $Username): self
    {
        $this->Username = $Username;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }

    public function setIsActive(bool $Is_active): self
    {
        $this->Is_active = $Is_active;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->Token;
    }

    public function setToken(string $Token): self
    {
        $this->Token = $Token;

        return $this;
    }

    public function getTokenExpire(): ?\DateTimeInterface
    {
        return $this->Token_Expire;
    }

    public function setTokenExpire(\DateTimeInterface $Token_Expire): self
    {
        $this->Token_Expire = $Token_Expire;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Like[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(Like $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
            $like->setUser($this);
        }

        return $this;
    }

    public function removeLike(Like $like): self
    {
        if ($this->likes->contains($like)) {
            $this->likes->removeElement($like);
            // set the owning side to null (unless already changed)
            if ($like->getUser() === $this) {
                $like->setUser(null);
            }
        }

        return $this;
    }

    public function getNewsletter(): ?Newsletter
    {
        return $this->newsletter;
    }

    public function setNewsletter(?Newsletter $newsletter): self
    {
        $this->newsletter = $newsletter;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $newsletter ? null : $this;
        if ($newsletter->getUser() !== $newUser) {
            $newsletter->setUser($newUser);
        }

        return $this;
    }

    public function getIsBan(): ?bool
    {
        return $this->Is_ban;
    }

    public function setIsBan(bool $Is_ban): self
    {
        $this->Is_ban = $Is_ban;

        return $this;
    }



}
