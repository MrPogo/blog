<?php


namespace App\Entity\User\User\ReadModel;


class UserRead
{
    private $id;
    private $email;
    private $password;
    private $Username;
    private $Is_active;
    private $Token;
    private $Token_Expire;
    private $roles;


    public function __construct(
        int $id,
        string $email,
        string $password,
        string $Username,
        bool $Is_active,
        ?string $Token,
        ?\DateTime $Token_Expire,
        array $Role
    )
    {

        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
        $this->Username = $Username;
        $this->Is_active = $Is_active;
        $this->Token = $Token;
        $this->Token_Expire = $Token_Expire;
        $this->Role = $Role;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getUsername(): string
    {
        return (string) $this->Username;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }
    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }
    public function getToken(): ?string
    {
        return $this->Token;
    }
    public function getTokenExpire(): ?\DateTimeInterface
    {
        return $this->Token_Expire;
    }
    public function getRoles(): array
    {

        // guarantee every user at least has ROLE_USER

        return (array) $this->roles;
    }
}