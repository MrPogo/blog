<?php


namespace App\Entity\User\User\UseCase;


use App\Adapter\Core\Transaction;
use App\Adapter\User\Users;
use App\Entity\User\User\UseCase\BanUser\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BanUser extends AbstractController
{

    private $transaction;
    private $emailFactory;
    private $mailer;


    public function __construct(Users $users, Transaction $transaction)
    {
        $this->users = $users;
        $this->transaction = $transaction;
    }

    public function execute(Command $command)
    {
        $this->transaction->begin();

        $User = $command->getUser();

        if($User->getIsBan()==true){
            $command->getResponder()->IsBan( $User);
            return;
        }

        $User->ban(true);

        try{
            $this->transaction->commit();
        } catch (\Throwable $e){
            $this->transaction->rollback();
            throw $e;
        }

        $command->getResponder()->ban($User);
    }

}