<?php


namespace App\Entity\User\User\UseCase;


use App\Adapter\Core\EmailFactory;
use App\Adapter\Core\Transaction;
use App\Adapter\User\Users;
use App\Entity\User\User;
use App\Entity\User\User\UseCase\ActiveUser\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ActiveUser extends AbstractController
{

        private $transaction;
    private $entityManager;
    private $emailFactory;
    private $mailer;


    public function __construct(Users $users, Transaction $transaction,
                                EmailFactory $emailFactory,
                                EntityManagerInterface $entityManager,
                                \Swift_Mailer $mailer)
    {
        $this->users = $users;
        $this->transaction = $transaction;
        $this->emailFactory = $emailFactory;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    public function execute(Command $command)
    {
        $this->transaction->begin();
        $User = $command->getUser();
        $User->active(
            $command->getIsActive(),
            $command->getToken(),
            $command->getTokenExpire()
        );


        try{
            $this->transaction->commit();
        } catch (\Throwable $e){
            $this->transaction->rollback();
            throw $e;
        }

        $command->getResponder()->UserActive($User);

        $template=$this->renderView("User/email/SuccesActiv.html.twig");
        $this->createNotFoundException();
        $url = $this->generateUrl('app_login');
        $template = str_replace("$.name.$",$User->getUsername(),$template);
        $swiftMessage = $this->emailFactory->create(
            'Pomyślna aktywacja',
            nl2br($template),
            [
                $User->getEmail()
            ]
        );
        $this->mailer->send($swiftMessage);

        $command->getResponder()->UserActive($User);

    }

}