<?php


namespace App\Entity\User\User\UseCase\ActiveUser;


use App\Entity\User\User;

interface Responder
{

    public function UserActive(User $user);
}