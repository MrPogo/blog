<?php


namespace App\Entity\User\User\UseCase\ActiveUser;


use App\Entity\User\User;
use DateTime;


class Command
{
    private $user;
    private $Is_active;
    private $Token;
    private $Token_Expire;
    private $responder;


    /**
     * Command constructor.
     * @param bool $Is_active
     * @param string $Token
     * @param DateTime $Token_Expire
     */
    public function __construct(
        User $user,
        bool $Is_active,
        ?string $Token,
        ?\DateTime $Token_Expire
    )
    {
        $this->user = $user;
        $this->Is_active = $Is_active;
        $this->Token = $Token;
        $this->Token_Expire = $Token_Expire;
        $this->responder = new NullResponder();
    }

    public function getUser(): User
    {
        return $this->user;
    }


    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }


    public function getToken(): ?string
    {
        return $this->Token;
    }


    public function getTokenExpire(): ?\DateTimeInterface
    {
        return $this->Token_Expire;
    }


    public function getResponder(): Responder
    {
        return $this->responder;
    }
    /**
     * @param Responder $responder
     */
    public function setResponder(Responder $responder): void
    {
        $this->responder = $responder;
    }

}