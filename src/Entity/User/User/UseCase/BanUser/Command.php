<?php


namespace App\Entity\User\User\UseCase\BanUser;


use App\Entity\User\User;

class Command
{

    private $User;
    private $Is_ban;
    private $responder;


    public function __construct(
        User $User
    )
    {

        $this->user = $User;
        $this->responder = new NullResponder();
    }


    public function getUser(): User
    {
        return $this->user;
    }

    public function getResponder()
    {
        return $this->responder;
    }

    public function setResponder($responder): void
    {
        $this->responder = $responder;
    }
}