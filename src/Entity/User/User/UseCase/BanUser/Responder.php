<?php


namespace App\Entity\User\User\UseCase\BanUser;


use App\Entity\User\User;

interface Responder
{
    public function ban(User $user);
    public function IsBan(User $user);

}