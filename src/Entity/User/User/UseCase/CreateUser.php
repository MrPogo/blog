<?php


namespace App\Entity\User\User\UseCase;


use App\Adapter\Core\EmailFactory;
use App\Adapter\Core\Transaction;
use App\Adapter\User\Users;
use App\Entity\User\User;
use App\Entity\User\User\UseCase\CreateUser\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGenerator;

class CreateUser extends AbstractController
{
    private $Users;
    private $transaction;
    private $entityManager;
    private $emailFactory;
    private $mailer;


    public function __construct(Users $users, Transaction $transaction,
                                EmailFactory $emailFactory,
                                EntityManagerInterface $entityManager,
                                    \Swift_Mailer $mailer )
    {
        $this->Users = $users;
        $this->transaction = $transaction;
        $this->emailFactory = $emailFactory;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;

    }

    public function execute(Command $command)
    {

        $this->transaction->begin();

        if($this->Users->findbyEmail($command->getEmail())){
            $command->getResponder()->emailExists();
            return;
        }

        if($this->Users->findbyUserName($command->getUsername())){
            $command->getResponder()->UserNameExists();
            return;
        }

        $Users = new User(
            $command->getEmail(),
            $command->getPassword(),
            $command->getUsername()


        );


        $this->Users->add($Users);

        try {
            $this->transaction->commit();
        } catch (\Throwable $e) {
            $this->transaction->rollback();
            throw $e;
        }

        $user=$this->entityManager->getRepository(User::class)->findOneBy(['email'=>$command->getEmail()]);
        $template=$this->renderView("User/email/Register.html.twig");
        $this->createNotFoundException();
        $url = $this->generateUrl('activ',array('token'=>$user->getToken()), UrlGenerator::ABSOLUTE_URL);
        $template = str_replace("$.name.$",$command->getUsername(),$template);
        $template = str_replace("$.LINK.$",'<a href="'.$url.'" target="_blank">aktywuj konto</a>',$template);
        $swiftMessage = $this->emailFactory->create(
            'Pomyślna rejestracja',
            nl2br($template),
            [
                $command->getEmail()
            ]
        );
        $this->mailer->send($swiftMessage);


        $command->getResponder()->CreateUser($Users);



    }
}