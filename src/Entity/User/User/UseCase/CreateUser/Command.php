<?php


namespace App\Entity\User\User\UseCase\CreateUser;



class Command
{
    private $id;
    private $email;
    private $roles = [];
    private $password;
    private $Username;
    private $Is_active;
    private $Token;
    private $Token_Expire;
    private $responder;


    /**
     * Command constructor.
     * @param $Username
     * @param $email
     * @param $password
     */
    public function __construct(
        string $email,
        string $password,
        string $Username
    )
    {

        $this->email = $email;
        $this->password = $password;
        $this->Username = $Username;
        $this->responder = new NullResponder();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): string
    {
        return (string) $this->Username;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER

        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function setUsername(string $Username): self
    {
        $this->Username = $Username;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }

    public function setIsActive(bool $Is_active): self
    {
        $this->Is_active = $Is_active;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->Token;
    }

    public function setToken(string $Token): self
    {
        $this->Token = $Token;

        return $this;
    }

    public function getTokenExpire(): ?\DateTimeInterface
    {
        return $this->Token_Expire;
    }

    public function setTokenExpire(\DateTimeInterface $Token_Expire): self
    {
        $this->Token_Expire = $Token_Expire;

        return $this;
    }

    public function getResponder(): Responder
    {
        return $this->responder;
    }

    /**
     * @param Responder $responder
     */
    public function setResponder(Responder $responder): void
    {
        $this->responder = $responder;
    }

}