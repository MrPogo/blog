<?php


namespace App\Entity\User;



interface UserInterface
{
    public function add(User $user);
    public function findbyToken(string $token);
    public function findbyEmail(string $email);
    public function findbyUserName(string $username);

}