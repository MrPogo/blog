<?php


namespace App\Entity\Permission;


use App\Entity\RolePermission\RolePermission;

interface PermissionInterface
{
    public function add(Permission $permission);
    public function  findByRolePermission(RolePermission $rolePermission);
    public function  findByName(string $name);
}