<?php


namespace App\Entity\Permission\ReadModel;


use App\Entity\Permission\Permission;
use App\Entity\RolePermission\RolePermission;

interface PermissionsQueryInterface
{
    public function getAll():array ;
    public function getById(int $id):Permission;
    public function getByRolePermission(RolePermission $rolePermission):array;
}