<?php


namespace App\Entity\Permission\ReadModel;


class PermissionReadModel
{
    private $id;
    private $name;
    private $function;

    public function __construct(int $id, string $name, string $function)
    {
        $this->id = $id;
        $this->name = $name;
        $this->function = $function;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFunction(): string
    {
        return $this->function;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}