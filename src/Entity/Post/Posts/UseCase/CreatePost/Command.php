<?php


namespace App\Entity\Post\Posts\UseCase\CreatePost;



use DateTime;

class Command

{
    private $message;
    private $data_add;
    private $is_active;
    private $Title;
    private $Responder;

    public function __construct(
        string $Title,
        string $message
    )
    {
        $this->Title=$Title;
        $this->message=$message;
        $this->Responder=new NullResponder();
    }


    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $Message): self
    {
        $this->Message = $Message;

        return $this;
    }

    public function getDataAdd(): ?\DateTimeInterface
    {
        return $this->data_add;
    }

    public function setDataAdd(\DateTimeInterface $data_add): self
    {
        $this->data_add = $data_add;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function setIsActive(bool $is_active): self
    {
        $this->is_active = $is_active;

        return $this;
    }
    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getResponder()
    {
        return $this->Responder;
    }


}