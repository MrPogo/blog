<?php


namespace App\Entity\Post\Posts\UseCase\CreatePost;


use App\Entity\Post\Post;

interface Responder
{
    public function CreatePost(Post $post);

}