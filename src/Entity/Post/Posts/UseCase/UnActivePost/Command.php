<?php


namespace App\Entity\Post\Posts\UseCase\UnActivePost;



use App\Entity\Post\Post;
use DateTime;

class Command

{
    private $Post;
    private $is_active;
    private $Responder;

    public function __construct(
        Post $Post,
    bool $is_active
    )
    {

        $this->Post = $Post;
        $this->is_active = $is_active;
        $this->Responder=new NullResponder();

    }


    public function getPost(): Post
{
    return $this->Post;
}

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function getResponder()
    {
        return $this->Responder;
    }


}