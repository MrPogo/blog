<?php


namespace App\Entity\Post\Posts\UseCase\UnActivePost;


use App\Entity\Post\Post;

interface Responder
{
    public function PostUnActive(Post $post);

}