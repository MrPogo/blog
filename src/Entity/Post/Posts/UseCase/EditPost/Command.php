<?php


namespace App\Entity\Post\Posts\UseCase\EditPost;



use App\Entity\Post\Post;

class Command

{
    private $Post;
    private $message;
    private $data_add;
    private $is_active;
    private $Title;
    private $Responder;

    public function __construct(
        Post $Post,
        string $Title,
        string $message
    )
    {
        $this->Post=$Post;
        $this->Title=$Title;
        $this->message=$message;
        $this->Responder=new NullResponder();

    }


    public function getPost(): Post
    {
        return $this->Post;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getDataAdd(): ?\DateTimeInterface
    {
        return $this->data_add;
    }

    public function getIsActive(): ?bool
    {
        return $this->is_active;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }


    public function setResponder(Responder $Responder): void
    {
        $this->Responder = $Responder;
    }
    public function getResponder()
    {
        return $this->Responder;
    }


}