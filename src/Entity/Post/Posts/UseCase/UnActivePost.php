<?php


namespace App\Entity\Post\Posts\UseCase;


use App\Adapter\Core\Transaction;
use App\Adapter\Post\Posts;
use App\Entity\Post\Posts\UseCase\UnActivePost\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UnActivePost extends AbstractController
{

    private $Posts;
    private $transaction;

    public function __construct(Posts $posts,
                                Transaction $transaction)
    {
        $this->Posts=$posts;
        $this->transaction=$transaction;
    }
    public function execute(Command $command)
    {
        $this->transaction->begin();
        $Post=$command->getPost();
        $Post->UnActive(
            $command->getIsActive()
        );


        try{
            $this->transaction->commit();
        } catch (\Throwable $e){
            $this->transaction->rollback();
            throw $e;
        }
        $command->getResponder()->PostUnActive($Post);

    }
}