<?php


namespace App\Entity\Post\Posts\UseCase;


use App\Adapter\Core\EmailFactory;
use App\Adapter\Core\Transaction;
use App\Adapter\Post\Posts;
use App\Entity\Post\Post;
use App\Entity\Post\Posts\UseCase\CreatePost\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CreatePost extends AbstractController
{

    private $Posts;
    private $transaction;
    private $emailFactory;
    private $entityManager;
    private $mailer;

    public function __construct(Posts $posts,
                                EmailFactory $emailFactory,
                                Transaction $transaction,
                                EntityManagerInterface $entityManager,
                                \Swift_Mailer $mailer )
    {
        $this->mailer = $mailer;
        $this->Posts=$posts;
        $this->transaction=$transaction;
        $this->emailFactory = $emailFactory;
        $this->entityManager = $entityManager;
    }
    public function execute(Command $command)
    {
        $this->transaction->begin();

        $Post = new Post(
            $command->getTitle(),
            $command->getMessage()

        );
        $this->Posts->add($Post);

        $newsletteremail = $this->entityManager->getRepository(\App\Entity\Newsletter\Newsletter::class)->findAll();

        $template=$this->renderView("Newsletter/email/NewsletterEmail.html.twig");

        foreach ($newsletteremail as $newsletteremai){
            $swiftMessage = $this->emailFactory->create(
                'nowy post',
                nl2br($template),
                [
                    $newsletteremai->getEmail()
                ]
            );
            $this->mailer->send($swiftMessage);
        }


        try{
            $this->transaction->commit();
        } catch (\Throwable $e){
            $this->transaction->rollback();
            throw $e;
        }

        $command->getResponder()->CreatePost($Post);

    }
}