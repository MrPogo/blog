<?php


namespace App\Entity\Post\Posts\ReadModel;


class PostsRead
{
private $id;
private $message;
private $data_add;
private $is_activ;
private $Title;

public function __construct(
    int $id,
    string $Title,
    string $message,
    \DateTime $data_add,
    bool $is_activ
)
{
    $this->id=$id;
    $this->Title=$Title;
    $this->message=$message;
    $this->data_add=$data_add;
    $this->is_activ=$is_activ;
}

public function getId():int
{
    return $this->id;
}
public function getmessage():string
{
    return $this->message;
}
public function getData_add():\DateTime
{
    return $this->data_add;
}
public function getIs_activ():bool
{
    return $this->is_activ;
}
    public function getTitle(): ?string
    {
        return $this->Title;
    }

}