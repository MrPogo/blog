<?php

namespace App\Entity\Comment;

use App\Entity\Post\Post;
use App\Entity\User\User;
use App\Repository\CommentRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Data_add;

    /**
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $post;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Is_active;

    public function __construct(
        string $comment,
        Post $post,
        User $user,
    bool $Is_active = true

    )
    {
        $this->comment=$comment;
        $this->Data_add=new DateTime("now");
        $this->post=$post;
        $this->user=$user;
        $this->Is_active = $Is_active;
    }

    public function UnActive (
        bool $is_active
    ){
        $this->Is_active=$is_active;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getDataAdd(): ?\DateTimeInterface
    {
        return $this->Data_add;
    }

    public function setDataAdd(\DateTimeInterface $Data_add): self
    {
        $this->Data_add = $Data_add;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }

    public function setIsActive(bool $Is_active): self
    {
        $this->Is_active = $Is_active;

        return $this;
    }
}
