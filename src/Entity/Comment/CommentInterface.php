<?php


namespace App\Entity\Comment;


interface CommentInterface
{

    public function add(Comment $comment);
}