<?php


namespace App\Entity\Comment\Comment\ReadModel;


use App\Entity\Post\Post;

class CommentReadModel
{
    private $id;
    private $comment;
    private $Data_add;
    private $post;
    private $user;
    private $Is_active;

    public function __construct(
       int $id,
    string $comment,
    \DateTime $Data_add,
    $post,
    bool $Is_active,
    $user
   )
   {
       $this->id=$id;
       $this->comment=$comment;
       $this->Data_add=$Data_add;
       $this->post=$post;
       $this->is_active = $Is_active;
       $this->user=$user;
   }

    public function getIsActive(): ?bool
    {
        return $this->Is_active;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getDataAdd(): \DateTime
    {
        return $this->Data_add;
    }


    public function getPost(): ?Post
    {
        return $this->post;
    }



    public function getUser(): ?string
    {
        return $this->user;
    }



}