<?php


namespace App\Entity\Comment\Comment\UseCase;


use App\Adapter\Comment\Comment;
use App\Adapter\Core\Transaction;
use App\Entity\Comment\Comment\UseCase\CreateComment\Command;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CreateComment extends AbstractController
{
    private $transaction;
    private $entityManager;
    private $Comment;

    public function __construct(Comment $comments,
                                 Transaction $transaction,
                                EntityManagerInterface $entityManager)
    {
        $this->Comment=$comments;
        $this->transaction=$transaction;
        $this->entityManager = $entityManager;
    }
    public function execute(Command $command)
    {
        $this->transaction->begin();

        $Comment = new \App\Entity\Comment\Comment(
            $command->getComment(),
            $command->getPost(),
            $command->getUser()

        );
        $this->Comment->add($Comment);



        try{
            $this->transaction->commit();
        } catch (\Throwable $e){
            $this->transaction->rollback();
            throw $e;
        }

        $command->getResponder()->CreateComment();

    }
}