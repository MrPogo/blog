<?php


namespace App\Entity\Comment\Comment\UseCase;


use App\Adapter\Comment\Comment;
use App\Adapter\Core\Transaction;
use App\Entity\Comment\Comment\UseCase\UnActiveComment\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UnActivecomment extends AbstractController
{

    private $Comments;
    private $transaction;

    public function __construct(Comment $comment,Transaction $transaction)
    {
        $this->Comments=$comment;
        $this->transaction=$transaction;

    }

    public function execute(Command $command)
    {
        $this->transaction->begin();
        $Comment=$command->getComment();
        $Comment->UnActive(false);

        try{
            $this->transaction->commit();
        } catch (\Throwable $e){
            $this->transaction->rollback();
            throw $e;
        }
        $command->getResponder()->CommentUnActive();
    }
}