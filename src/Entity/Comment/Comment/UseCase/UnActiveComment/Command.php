<?php


namespace App\Entity\Comment\Comment\UseCase\UnActiveComment;


use App\Entity\Comment\Comment;

class Command
{

    private $Comment;
    private $is_active;
    private $Responder;


    public function __construct(
        Comment $Comment
    )
    {
        $this->Comment=$Comment;
        $this->Responder=new NullResponder();

    }

    public function getComment() :Comment
    {
        return $this->Comment;
    }



    public function getResponder():Responder
    {
        return $this->Responder;
    }


    public function setResponder($Responder): void
    {
        $this->Responder = $Responder;
    }

}