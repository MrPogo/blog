<?php


namespace App\Entity\Comment\Comment\UseCase\CreateComment;


interface Responder
{
    public function CreateComment();

}