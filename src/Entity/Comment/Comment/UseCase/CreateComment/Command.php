<?php


namespace App\Entity\Comment\Comment\UseCase\CreateComment;


use App\Entity\Post\Post;
use App\Entity\User\User;

class Command
{
    private $comment;
    private $Data_add;
    private $post;
    private $user;
    private $responder;

    public function __construct(
        string $comment,
        Post $post,
        User $user
    )
    {
        $this->comment=$comment;
        $this->post=$post;
        $this->user=$user;
        $this->responder=new NullResponder();
    }



    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getDataAdd(): ?\DateTimeInterface
    {
        return $this->Data_add;
    }


    public function getPost(): ?Post
    {
        return $this->post;
    }



    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getResponder(): Responder
    {
        return $this->responder;
    }

    /**
     * @param Responder $responder
     */
    public function setResponder(Responder $responder): void
    {
        $this->responder = $responder;
    }
}