<?php


namespace App\Entity\Role;


interface RoleInterface
{
    public function add(Role $role);
    public function  findByName(string $name);
}