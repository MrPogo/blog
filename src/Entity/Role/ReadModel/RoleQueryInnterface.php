<?php


namespace App\Entity\Role\ReadModel;


use App\Entity\Role\Role;

interface RoleQueryInnterface
{
    public function getAll():array ;
    public function getById(int $id):Role;

    public function getByName(string $name):Role;
}