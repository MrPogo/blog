<?php


namespace App\Adapter\Comment;


use Doctrine\ORM\EntityManagerInterface;

final class Comment
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function add(\App\Entity\Comment\Comment $comment)
    {
        $this->manager->persist($comment);
    }
    public function findbyId(int $Id)
    {
        return $this->manager->getRepository('App:Comment\Comment')->findOneBy(['id'=>$Id]);
    }
}