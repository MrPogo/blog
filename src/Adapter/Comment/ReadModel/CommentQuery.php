<?php


namespace App\Adapter\Comment\ReadModel;



use App\Adapter\Comment\Comment;
use App\Adapter\Comment\CommentQueryInterface;
use App\Entity\Comment\Comment\ReadModel\CommentReadModel;
use DateTime;
use Doctrine\DBAL\Driver\Connection;


class CommentQuery implements CommentQueryInterface
{
    private $connection;
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
    /**
     * @return Comment[]
     */
    public function getAllByIdPost(int $IdPost)
    {
        return $this->connection->project(
            'SELECT c.id as id, c.post_id as post_id, c.comment as comment,c.data_add as 	data_add,c.is_active as is_active, u.username as username
        FROM comment as c
        LEFT JOIN user as u ON c.user_id=u.id
        WHERE c.post_id = :id_post
        ORDER BY c.data_add DESC ',
            [

                'id_post'=>$IdPost
            ],
            function (array $result) {
                return new CommentReadModel(
                    (int) $result['id'],
                    (string) $result['comment'],
                    (new DateTime ($result['data_add'])),
                    $result['post_id'],
                    $result['is_active'],
                    (string)$result['username']
                );

            }

        );
    }
    public function getlast10day()
    {
        $paymentDate= new DateTime('now');
        $paymentDate->modify('-10 day');

        return $this->connection->project(
            'SELECT c.id as id, c.post_id as post_id, c.comment as comment,c.data_add as 	data_add,c.is_active as is_active, u.username as username
        FROM comment as c
        LEFT JOIN user as u ON c.user_id=u.id
        WHERE c.data_add > :data_add
        ORDER BY c.data_add DESC ',
            [

                'data_add'=>$paymentDate->format('Y-m-d')
            ],
            function (array $result) {
                return new CommentReadModel(
                    (int) $result['id'],
                    (string) $result['comment'],
                    (new DateTime ($result['data_add'])),
                    $result['post_id'],
                    $result['is_active'],
                    (string)$result['username']
                );

            }

        );
    }

}