<?php


namespace App\Adapter\Comment;


interface CommentQueryInterface
{
    /**
     * @return Comment[]
     */
    public function getAllByIdPost(int $IdPost);
    public function getlast10day();
}