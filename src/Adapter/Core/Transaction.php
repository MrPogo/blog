<?php


namespace App\Adapter\Core;


use App\Core\TransactionInterface;
use Doctrine\ORM\EntityManagerInterface;

class Transaction implements TransactionInterface
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function begin()
    {
        $this->em->beginTransaction();
    }

    public function commit()
    {
        $this->em->flush();
        $this->em->commit();
    }

    public function rollBack()
    {
        $this->em->rollback();
    }
}