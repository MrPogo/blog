<?php


namespace App\Adapter\User;


use App\Entity\User\User;
use App\Entity\User\UserInterface;
use Doctrine\ORM\EntityManagerInterface;

class Users implements UserInterface
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function add(User $user)
    {
        $this->manager->persist($user);
    }

    public function findbyToken(string $token)
    {
        return $this->manager->getRepository('App:User\User')->findOneBy(['Token'=>$token]);
    }

    public function findbyEmail(string $email)
    {
        return $this->manager->getRepository('App:User\User')->findOneBy(['email'=>$email]);
    }
    public function findbyUserName(string $username)
    {
        return $this->manager->getRepository('App:User\User')->findOneBy(['Username'=>$username]);
    }
}