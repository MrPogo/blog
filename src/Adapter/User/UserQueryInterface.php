<?php


namespace App\Adapter\User;


interface UserQueryInterface
{
    public function getByToken(string $token);
    public function getByEmail(string $email);
    public function getByLogin(string $email,string $password);
}