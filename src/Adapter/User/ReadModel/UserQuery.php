<?php


namespace App\Adapter\User\ReadModel;



use App\Adapter\User\UserQueryInterface;
use App\Entity\User\User;
use DateTime;
use Doctrine\DBAL\Driver\Connection;

class UserQuery implements UserQueryInterface
{
    private $connection;
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
 * @return User
 */
    public function getByToken(string $token)
    {
        return $this->connection->project(
            'SELECT u.id as id, u.email as email,u.password as password,u.username as username,u.is_active as is_active,u.token as token,u.token_expire as token_expire, u.roles as roles
                FROM user as u
                WHERE u.token = :token',
            [
                'token'=>$token
            ],
            function ($result) {
                return new User\ReadModel\UserRead(
                    (int) $result['id'],
                    (string) $result['email'],
                    (string) $result['password'],
                    (string) $result['username'],
                    (bool) $result['is_active'],
                    (string) $result['token'],
                    (new DateTime ($result['token_expire'])),
                    (array) $result['roles']
                );

            }
        );
    }

    public function getByEmail(string $email)
    {
        return $this->connection->project(
            'SELECT u.id as id, u.email as email,u.password as password,u.username as username,u.is_active as is_active,u.token as token,u.token_expire as token_expire, u.roles as roles
                FROM user as u
                WHERE u.email = :email',
            [
                'email'=>$email
            ],
            function ($result) {
                return new User\ReadModel\UserRead(
                    (int) $result['id'],
                    (string) $result['email'],
                    (string) $result['password'],
                    (string) $result['username'],
                    (bool) $result['is_active'],
                    (string) $result['token'],
                    (new DateTime ($result['token_expire'])),
                    (array) $result['roles']
                );

            }
        );
    }
    public function getByLogin(string $email, string $password)
    {
        return $this->connection->project(
            'SELECT u.id as id, u.email as email,u.password as password,u.username as username,u.is_active as is_active,u.token as token,u.token_expire as token_expire, u.roles as roles
                FROM user as u
                WHERE u.email = :email and u.password = :password',
            [
                'email'=>$email,
                'password'=>$password
            ],
            function ($result) {
                return new User\ReadModel\UserRead(
                    (int) $result['id'],
                    (string) $result['email'],
                    (string) $result['password'],
                    (string) $result['username'],
                    (bool) $result['is_active'],
                    (string) $result['token'],
                    (new DateTime ($result['token_expire'])),
                    (array) $result['roles']
                );

            }
        );
    }
}