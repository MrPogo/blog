<?php


namespace App\Adapter\Like\ReadModel;


use App\Adapter\Like\Like;
use App\Adapter\Like\LikeQueryInterface;
use App\Entity\Like\Like\ReadModel\LikeReadModel;
use DateTime;
use Doctrine\DBAL\Driver\Connection;


class LikeQuery implements LikeQueryInterface
{private $connection;
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
    /**
     * @return Like[]
     */

    public function findLikeInPost(int $postID)
    {
        return $this->connection->project(
            'SELECT l.id as id, l.user_id  as user_id , l.post_id  as post_id ,l.is_active as is_active,l.data_add as 	data_add
                FROM `like` as l
                WHERE l.post_id = :postID ',
            [
                'postID'=>$postID
            ],
            function (array $result) {
                return new LikeReadModel(
                    (int) $result['id'],
                    (int) $result['user_id'],
                    (int) $result['post_id'],
                    (bool) $result['is_active'],
                    (new DateTime ($result['data_add']))
                );

            }
        );    }

        public function last10day()
        {

            $paymentDate= new DateTime('now');
            $paymentDate->modify('-10 day');

            return $this->connection->project(
                'SELECT l.id as id, l.user_id  as user_id , l.post_id  as post_id ,l.is_active as is_active,l.data_add as 	data_add
                FROM `like` as l
                WHERE l.data_add > :data_add ',
                [

                    'data_add'=>$paymentDate->format('Y-m-d')
                ],
                function (array $result) {
                    return new LikeReadModel(
                        (int) $result['id'],
                        (int) $result['user_id'],
                        (int) $result['post_id'],
                        (bool) $result['is_active'],
                        (new DateTime ($result['data_add']))
                    );

                }
            );
        }

}