<?php


namespace App\Adapter\Like;


use App\Entity\Like\LikeInterface;
use App\Entity\Post\Post;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;


final class Like implements LikeInterface
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }
    public function add(\App\Entity\Like\Like $like)
    {
        $this->manager->persist($like);
    }
    public function delete(\App\Entity\Like\Like $like)
    {
        $this->manager->remove($like);
    }
    public function findLikeUserInPost(int $postID, int $userID)
    {
        return $this->manager->getRepository(\App\Entity\Like\Like::class)->findOneBy(['Post'=>$postID,'User'=>$userID]);
    }
}