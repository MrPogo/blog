<?php


namespace App\Adapter\Like;


interface LikeQueryInterface
{
    /**
     * @return \App\Entity\Like\Like[]
     */
    public function findLikeInPost(int $postID);
    public function last10day();

}