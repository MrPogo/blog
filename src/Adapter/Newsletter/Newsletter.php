<?php


namespace App\Adapter\Newsletter;


use App\Entity\Newsletter\NewsletterInterface;
use Doctrine\ORM\EntityManagerInterface;

final class Newsletter implements  NewsletterInterface
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function add(\App\Entity\Newsletter\Newsletter $newsletter)
    {
        $this->manager->persist($newsletter);
    }

    public function getAll(\App\Entity\Newsletter\Newsletter $newsletter)
    {
        return $this->manager->getRepository('App:Newsletter\Newsletter')->findAll();
    }
    public function findOneByEmail(string $email)
    {
        return $this->manager->getRepository('App:Newsletter\Newsletter')->findOneBy(['email'=>$email]);
    }
    public function delete(\App\Entity\Newsletter\Newsletter $newsletter)
    {
        $this->manager->remove($newsletter);
    }

}