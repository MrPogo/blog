<?php


namespace App\Adapter\Post;


interface PostsQueryInterface
{
/**
 * @return Posts[]
 */
public function getAll();
public function getAllActiv(bool $is_activ);
public function getByIdActiv(bool $is_activ,int $id);

}