<?php

namespace App\Adapter\Post;


use App\Entity\Post\Post;
use App\Entity\Post\PostsInterface;
use Doctrine\ORM\EntityManagerInterface;

final class Posts implements PostsInterface
{
    private $manager;
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function add(Post $post)
    {
        $this->manager->persist($post);
    }

    public function fundbyid(int $PostID)
    {
        return $this->manager->getRepository('App:Post\Post')->findOneBy(['id'=>$PostID]);
    }


}