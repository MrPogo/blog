<?php


namespace App\Adapter\Post\ReadModel;


use App\Adapter\Post\Posts;
use App\Adapter\Post\PostsQueryInterface;
use App\Entity\Post\Posts\ReadModel\PostsRead;
use DateTime;
use Doctrine\DBAL\Driver\Connection;

class PostsQuery implements PostsQueryInterface
{
    private $connection;
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
    /**
     * @return Posts[]
     */
    public function getAll()
    {
        return $this->connection->project(
            'SELECT p.id as id, p.message as message, p.data_add as data_add,p.is_active as is_active,p.title as 	title
                FROM post as p',
            [
            ],
            function (array $result) {
                return new PostsRead(
                    (int) $result['id'],
                    (string) $result['title'],
                    (string) $result['message'],
                    (new DateTime ($result['data_add'])),
                    (bool) $result['is_active']
                );

            }
        );
    }
    public function getAllActiv(bool $is_activ)
    {
        return $this->connection->project(
        'SELECT p.id as id, p.message as message, p.data_add as data_add,p.is_active as is_active,p.title as 	title
                FROM post as p
                WHERE p.is_active = :is_active 
                ORDER BY p.data_add DESC ',
        [
            'is_active' => $is_activ,
        ],
        function (array $result) {
            return new PostsRead(
            (int) $result['id'],
            (string) $result['title'],
            (string) $result['message'],
            (new DateTime ($result['data_add'])),
            (bool) $result['is_active']
        );

            }
        );
    }
    public function getByIdActiv(bool $is_activ, int $id)
    {
        return $this->connection->project(
            'SELECT p.id as id, p.message as message, p.data_add as data_add,p.is_active as is_active,p.title as 	title
        FROM post as p
        WHERE p.is_active = :is_active and p.id= :ID',
            [

                'is_active' => $is_activ,
                'ID'=>$id
            ],
            function (array $result) {
                return new PostsRead(
                    (int) $result['id'],
                    (string) $result['title'],
                    (string) $result['message'],
                    (new DateTime ($result['data_add'])),
                    (bool) $result['is_active']
                );

            }

        );
    }
}