<?php


namespace App\Controller\Admin;


use App\Adapter\Comment\ReadModel\CommentQuery;
use App\Adapter\Like\ReadModel\LikeQuery;
use DateTime;
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Layout;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class savefileController extends AbstractController
{




    /**
     * @Route("/admin/savefile", name="savefile")
     */

    function main(CommentQuery $commentQuery, LikeQuery $likeQuery, Request $request)
    {
        $Comments=$commentQuery->getlast10day();
        $comments=$this->Transfer($Comments);
        $lastdata=$this->arraydata();

        $Likes=$likeQuery->last10day();
        $likes=$this->Transfer($Likes);


        $spreadsheet = new Spreadsheet();
        $worksheet = $spreadsheet->getActiveSheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();


        array_unshift($lastdata, 'Data');
        array_unshift($comments, 'Comments');
        array_unshift($likes, 'Like');


        $worksheet->fromArray(
            [
                $lastdata,
                $comments,
                $likes,
            ]
        );


        $dataSeriesLabels1 = [
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'Worksheet!$B$1:$K$1', null, 10), // 2011
        ];

        $xAxisTickValues1 = [
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'Worksheet!$A$2', null, 1),
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_STRING, 'Worksheet!$A$3', null, 1)// Q1 to Q4
        ];

        $dataSeriesValues1 = [
            new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'Worksheet!$B$2:$K$2', null, 10), new DataSeriesValues(DataSeriesValues::DATASERIES_TYPE_NUMBER, 'Worksheet!$B$3:$K$3', null, 10)
        ];

        $series1 = new DataSeries(
            DataSeries::TYPE_BARCHART, // plotType
            null, // plotGrouping (Pie charts don't have any grouping)
            range(0, count($dataSeriesLabels1)), // plotOrder
            $xAxisTickValues1,
            $dataSeriesLabels1,
            $dataSeriesValues1

        );

        $layout1 = new Layout();
        $layout1->setShowVal(true);
        $layout1->setShowPercent(true);

// Set the series in the plot area
        $plotArea1 = new PlotArea($layout1, [$series1]);
// Set the chart legend
        $legend1 = new Legend(Legend::POSITION_RIGHT, null, false);

        $title1 = new Title('raport');

// Create the chart
        $chart1 = new Chart(
            'chart1', // name
            $title1, // title
            $legend1, // legend
            $plotArea1, // plotArea
            true, // plotVisibleOnly
            DataSeries::EMPTY_AS_GAP, // displayBlanksAs
            null, // xAxisLabel
            null   // yAxisLabel - Pie charts don't have a Y-Axis
        );

// Set the position where the chart should appear in the worksheet
        $chart1->setTopLeftPosition('A7');
        $chart1->setBottomRightPosition('Q40');

// Add the chart to the worksheet
        $worksheet->addChart($chart1);


        $paymentDate = new DateTime('now');

        // Create a Temporary file in the system
        $fileName = 'Raport' . $paymentDate->format('Y-m-d h:i:s') . '.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->setIncludeCharts(true);
        $callStartTime = microtime(true);
        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);


    }


    public function arraydata()
    {



        for ($i=9;$i>=0;$i--)
        {
            $paymentDate= new DateTime('now');
            $paymentDate->modify("-$i day");
            $lastdata[]=$paymentDate->format('Y-m-d');

        }


        return $lastdata;
    }
    public function Transfer(array $Comments)
    {


        for ( $i=0;$i<10;$i++)
        {
            $paymentDate= new DateTime('now');
            $paymentDate->modify("-$i day");

            $comments[$paymentDate->format('Y-m-d')][]=null;

        }

        foreach ($Comments as $comment)
        {
            for ( $i=9;$i>=0;$i--)
            {
                $paymentDate= new DateTime('now');
                $paymentDate->modify("-$i day");
                if($comment->getDataAdd()->format('Y-m-d')==$paymentDate->format('Y-m-d'))
                {
                    $comments[$paymentDate->format('Y-m-d')][]=$comment;
                }
            }

        }

        for ($i=9;$i>=0;$i--)
        {
            $paymentDate= new DateTime('now');
            $paymentDate->modify("-$i day");
            $countcomments[]= count($comments[$paymentDate->format('Y-m-d')])-1;



        }


        return ($countcomments);
    }
}
