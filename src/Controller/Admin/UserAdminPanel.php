<?php


namespace App\Controller\Admin;



use App\Adapter\Comment\ReadModel\CommentQuery;
use App\Adapter\Like\ReadModel\LikeQuery;
use App\Form\Admin\Export;
use DateTime;
use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Layout;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class UserAdminPanel extends AbstractController
{

    /**
     * @Route("/admin/dashboard", name="Dashboard ")
     * @param CommentQuery $commentQuery
     * @param LikeQuery $likeQuery
     * @param Request $request
     */
    public function main(CommentQuery $commentQuery, LikeQuery $likeQuery, Request $request){

        $Comments=$commentQuery->getlast10day();
        $comments=$this->Transfer($Comments);
        $lastdata=$this->arraydata();

        $Likes=$likeQuery->last10day();
        $likes=$this->Transfer($Likes);

        if ($this->getUser()->getRoles()!=["ROLE_ADMIN"]) {
            return $this->redirectToRoute('homepage');
        }



        return $this->render('Admin/dashbored.html.twig',['packages' => $comments,'lastdata' => $lastdata,'likes' => $likes]);


    }
    public function arraydata()
    {



        for ($i=9;$i>=0;$i--)
        {
            $paymentDate= new DateTime('now');
            $paymentDate->modify("-$i day");
            $lastdata[]=$paymentDate->format('Y-m-d');

        }


        return $lastdata;
    }
    public function Transfer(array $Comments)
    {


        for ( $i=0;$i<10;$i++)
        {
            $paymentDate= new DateTime('now');
            $paymentDate->modify("-$i day");

                $comments[$paymentDate->format('Y-m-d')][]=null;

        }

        foreach ($Comments as $comment)
        {
            for ( $i=9;$i>=0;$i--)
            {
                $paymentDate= new DateTime('now');
                $paymentDate->modify("-$i day");
                if($comment->getDataAdd()->format('Y-m-d')==$paymentDate->format('Y-m-d'))
                {
                    $comments[$paymentDate->format('Y-m-d')][]=$comment;
                }
            }

        }

            for ($i=9;$i>=0;$i--)
            {
                $paymentDate= new DateTime('now');
                $paymentDate->modify("-$i day");
                $countcomments[]= count($comments[$paymentDate->format('Y-m-d')])-1;



            }


            return ($countcomments);
    }

}