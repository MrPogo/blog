<?php


namespace App\Controller\Newsletter;


use App\Adapter\Newsletter\Newsletter;
use App\Adapter\User\Users;
use App\Entity\Newsletter\Newsletter\UseCase\CreateNewsleter;
use App\Entity\Newsletter\Newsletter\UseCase\DeleteNewsletter;
use App\Entity\User\User;
use App\Entity\User\UserInterface;
use App\Form\Newsletter\Join_delete_userNewsletter;
use App\Form\Newsletter\JointoNewsletterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Newsletter\Newsletter\UseCase\CreateNewsleter\Command as CreateNewsletterCommand;
use App\Entity\Newsletter\Newsletter\UseCase\DeleteNewsletter\Command as DeleteNewsletterCommand;

class NewsleterJoinControler extends AbstractController
{
    private $entityManager;
    private $Newsletter;

    public function __construct(EntityManagerInterface $entityManager, Newsletter $newsletter)
    {
        $this->entityManager = $entityManager;
        $this->Newsletter = $newsletter;
    }

    /**
     * @param Request $request
     * @param CreateNewsleter $createNewsleter
     * @param DeleteNewsletter $deleteNewsletter
     * @param Newsletter $newsletter
     * @return Response
     * @Route("/newsletter", name="newsletter")
     */
    public function main(Request $request, CreateNewsleter $createNewsleter, DeleteNewsletter $deleteNewsletter, Newsletter $newsletter,Users $User):Response
    {
        if ($this->getUser()) {
            $user=$User->findbyEmail($this->getUser()->getEmail());
            if ($this->Newsletter->findOneByEmail($user->getEmail())) {
                $form = $this->createForm(Join_delete_userNewsletter::class);
                $form->handleRequest($request);
                if ($form->isSubmitted()) {
                    $formData = $form->getData();
                    $commendNewsletter = new DeleteNewsletter\Command($this->Newsletter->findOneByEmail($user->getEmail()));
                    $deleteNewsletter->execute($commendNewsletter);


                    return $this->redirectToRoute('homepage');


                }
                return $this->render('Newsletter/DeleteUserNewsletter.html.twig', [
                    'form' => $form->createView(),
                ]);
            } else {
                $form = $this->createForm(Join_delete_userNewsletter::class);
                $form->handleRequest($request);
                if ($form->isSubmitted()) {
                    $formData = $form->getData();

                    $commendNewsletter = new CreateNewsletterCommand($user->getEmail(), $user, true);
                    $createNewsleter->execute($commendNewsletter);


                    return $this->redirectToRoute('homepage');

                }
                return $this->render('Newsletter/JoinUserToNewsletter.html.twig', [
                    'form' => $form->createView(),
                ]);
            }
        } else {
            $form = $this->createForm(JointoNewsletterType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $formData = $form->getData();

                $commendNewsletter = new CreateNewsletterCommand($formData['email'], NULL, true);
                $createNewsleter->execute($commendNewsletter);


                return $this->redirectToRoute('homepage');

            }
            return $this->render('Newsletter/JoinToNewsletter.html.twig', [
                'form' => $form->createView(),
            ]);

        }


    }
}