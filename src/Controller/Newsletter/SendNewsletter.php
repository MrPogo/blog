<?php


namespace App\Controller\Newsletter;



use App\Adapter\Core\EmailFactory;
use App\Adapter\Core\Transaction;
use App\Adapter\Post\Posts;
use App\Form\Newsletter\SendNewsletterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SendNewsletter extends AbstractController
{
    private $emailFactory;
    private $entityManager;
    private $mailer;

    public function __construct(
                                EmailFactory $emailFactory,
                                EntityManagerInterface $entityManager,
                                \Swift_Mailer $mailer )
    {
        $this->mailer = $mailer;
        $this->emailFactory = $emailFactory;
        $this->entityManager = $entityManager;
    }


    /**
     * @Route("/send", name="sendNewsletter")
     */
    public function main (Request $request)
    {
        if ($this->getUser()->getRoles()!=["ROLE_ADMIN"]) {
            return $this->redirectToRoute('homepage');
        }
        $form=$this->createForm(SendNewsletterType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $formData = $form->getData();
            $newsletteremail = $this->entityManager->getRepository(\App\Entity\Newsletter\Newsletter::class)->findAll();

            foreach ($newsletteremail as $newsletteremai){
                $swiftMessage = $this->emailFactory->create(
                    $formData['Title'],
                    nl2br($formData['Message']),
                    [
                        $newsletteremai->getEmail()
                    ]
                );
                $this->mailer->send($swiftMessage);
            }

        }
        return $this->render('Newsletter/SendNewsletter.html.twig', [
            'form' => $form->createView(),
        ]);

    }

}