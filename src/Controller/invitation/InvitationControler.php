<?php


namespace App\Controller\invitation;


use App\Adapter\Core\EmailFactory;
use App\Form\invitation\InvitationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;

class InvitationControler extends AbstractController
{
    private $emailFactory;
    private $entityManager;
    private $mailer;

    public function __construct(
        EmailFactory $emailFactory,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer )
    {
        $this->mailer = $mailer;
        $this->emailFactory = $emailFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin/invitation", name="invitation")
     */

    public function main(Request $request)
    {
        if ($this->getUser()->getRoles()!=["ROLE_ADMIN"]) {
            return $this->redirectToRoute('homepage');
        }
        $form=$this->createForm(InvitationType::class);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid()) {
            $template=$this->renderView("invitation/Email/Invitation.html.twig");
            $url = $this->generateUrl('register');
            $template = str_replace("$.LINK.$",'<a href="'.$url.'">Zarejestruj się</a>',$template);
            $formData = $form->getData();
                $swiftMessage = $this->emailFactory->create(
                    'Zaproszenie',
                    nl2br($template),
                    [
                        $formData['email']
                    ]
                );
                $this->mailer->send($swiftMessage);
            }


        return $this->render('invitation/Invitation.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}