<?php


namespace App\Controller\User;


use App\Adapter\User\Users;
use App\Entity\User\User;
use App\Entity\User\User\UseCase\ActiveUser\Command;
use App\Entity\User\User\UseCase\ActiveUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User\User\UseCase\ActiveUser\Responder as ActiveUserResponder;


class ActivateUserControler extends AbstractController implements ActiveUserResponder
{


    /**
     * @param string $token
     * @throws \Exception
     * @Route("/activ/{token}", name="activ", methods={"GET", "POST"})
     */

    public function activ(string $token,Users $User,ActiveUser $activeUser,\Swift_Mailer $mailer)
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('homepage');
        }

        $user=$User->findbyToken($token);
            $date = new \DateTime("now");
            if ($user->getIsActive() == 0) {
                dump($user);
                $this->createNotFoundException();
                if ($user->getTokenExpire()->getTimestamp() > $date->getTimestamp()) {
                    $commendData = new Command(
                        $user,
                        true,
                        NULL,
                        NULL);
                    $commendData->setResponder($this);
                    $activeUser->execute($commendData);


                    return $this->redirectToRoute('homepage');
                } else {
                    return $this->redirectToRoute('token_expire');
                }
            } else {
                return $this->redirectToRoute('homepage');

            }


    }

    public function UserActive(User $user)
    {
    $this->addFlash('success','Użytkownik '.$user->getUsername().' został Aktywowany');
    }
}