<?php


namespace App\Controller\User;


use App\Adapter\User\Users;
use App\Entity\User\User;
use App\Entity\User\User\UseCase\BanUser\Command;
use App\Entity\User\User\UseCase\BanUser\Responder;
use App\Form\User\BanType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BanUser extends AbstractController implements Responder
{
    /**
     * @param Request $request
     * @throws \Exception
     * @Route("/admin/ban", name="ban", methods={"GET", "POST"})
     */
    public function main(Request $request,Users $User,\App\Entity\User\User\UseCase\BanUser $banUser){

        $form=$this->createForm(BanType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $formData = $form->getData();
            $user=$User->findbyUserName($formData['UserName']);
            $commendData = new Command($user);
            $commendData->setResponder($this);
            $banUser->execute($commendData);




        }
        return $this->render('User/Ban.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    public function ban(User $user)
    {
        $this->addFlash('success','Użytkownik '.$user->getUsername().' został zablokowany');
    }
    public function IsBan(User $user)
    {
        $this->addFlash('error','Użytkownik '.$user->getUsername().' jest już zablokowany');    }
}