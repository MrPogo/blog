<?php


namespace App\Controller\User;


use App\Adapter\User\Users;
use App\Entity\User\User;
use App\Entity\User\User\UseCase\PasswordChange\Responder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User\User\UseCase\PasswordChange\Command as PasswordChangeCommand;

class PasswordChange extends AbstractController implements Responder
{

    /**
     * @param Request $request
     * @throws \Exception
     * @Route("/password/{token}", name="app_password_change", methods={"GET", "POST"})
     */
    public function main(string $token,Request $request,Users $User,User\UseCase\PasswordChange $passwordChange )
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('homepage');
        }


        $form=$this->createForm(\App\Form\User\PasswordChange::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $user=$User->findbyToken($token);
            $commendData= new PasswordChangeCommand($user,$form->get('plainPassword')->getData(),null,null);
            $passwordChange->execute($commendData);


            return $this->redirectToRoute('homepage');
        }

        return $this->render('User/PasswordChange.html.twig', [
            'form' => $form->createView(),
        ]);


    }

    public function PasswordChange()
    {
        $this->addFlash('success','Hasło zostało zmienione');
    }

    public function LostToken()
    {
        $this->addFlash('error','Token wygasł');
    }



}