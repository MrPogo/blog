<?php


namespace App\Controller\User;


use App\Adapter\User\Users;
use App\Entity\User\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User\User\UseCase\PasswordReset\Command as PasswordResetCommand;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User\User\UseCase\PasswordReset\Responder as PasswordResetResponder;
use Symfony\Component\Routing\Annotation\Route;


class PasswordReset extends AbstractController  implements PasswordResetResponder
{
    /**
     * @param Request $request
     * @throws \Exception
     * @Route("/Password", name="app_PasswordReset", methods={"GET", "POST"})
     */
    public function main(Request $request,\App\Entity\User\User\UseCase\PasswordReset $passwordReset,Users $User)
    {

        if ($this->getUser()) {
            return $this->redirectToRoute('homepage');
        }

        $form = $this->createForm(\App\Form\User\PasswordReset::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $user=$User->findbyEmail($formData['email']);
            $commendData = new PasswordResetCommand( $user, md5(uniqid(time())), new DateTime('+15 minutes'));
            $commendData->setResponder($this);

            $passwordReset->execute($commendData);


            return $this->redirectToRoute('homepage');

        }
        return $this->render('User/PasswordReset.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function passwordreset(User $user)
    {
        // TODO: Implement passwordreset() method.
    }

}