<?php


namespace App\Controller\User;


use App\Adapter\User\ReadModel\UserQuery;
use App\Adapter\User\Users;
use App\Entity\Newsletter\Newsletter\UseCase\CreateNewsleter;
use App\Entity\User\User;
use App\Entity\User\User\UseCase\CreateUser\Command as CreateUserCommand;
use App\Form\User\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User\User\UseCase\CreateUser\Responder as CreateUserResponder;
use App\Entity\Newsletter\Newsletter\UseCase\CreateNewsleter\Command as CreateNewsletterCommand;

class CreateUser extends AbstractController implements CreateUserResponder
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/register", name="register")
     */
    public function main(Request $request,\App\Entity\User\User\UseCase\CreateUser $user,Users $User ,CreateNewsleter $createNewsleter ):Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('homepage');
        }

        $form=$this->createForm(RegisterType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $formData = $form->getData();
            // fail authentication with a custom error
            $commendData = new CreateUserCommand(
                $formData['email'],
                $form->get('plainPassword')->getData(),
                $formData['username']
            );
            $commendData->setResponder($this);

            $user->execute($commendData);
            $user = $User->findbyEmail($formData['email']);
            if ($user != null){
                if (($user->getUsername() == $formData['username']) && ($user->getEmail() == $formData['email'])) {
                    $commendNewsletter = new CreateNewsletterCommand($user->getEmail(), $user, true);
                    $createNewsleter->execute($commendNewsletter);
                }
        }

                return $this->redirectToRoute('homepage');
        }
        return $this->render('User/Register.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    public function CreateUser(User $user)
    {
        $this->addFlash('success','Użytkownik '.$user->getUsername().' jest zarejestrowany');
    }

    public function emailExists()
    {
        $this->addFlash('error','email jest zajęty');
    }
    public function UserNameExists()
    {
        $this->addFlash('error','Username jest zajęty');
    }
}