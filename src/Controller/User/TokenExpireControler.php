<?php


namespace App\Controller\User;


use App\Adapter\User\Users;
use App\Entity\User\User;
use App\Entity\User\User\UseCase\TokenExpire\Command;
use App\Form\User\TokenExpireType;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User\User\UseCase\TokenExpire\Responder as TokenExpireResponder;

class TokenExpireControler extends AbstractController implements TokenExpireResponder
{

    /**
     * @param Request $request
     * @throws \Exception
     * @Route("/token_expire", name="token_expire", methods={"GET", "POST"})
     */

    public function TokenExpire(Request $request,Users $User,User\UseCase\TokenExpire $tokenExpire,\Swift_Mailer $mailer)
    {

        if ($this->getUser()) {
            return $this->redirectToRoute('homepage');
        }


        $form=$this->createForm(TokenExpireType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $formData = $form->getData();
            $user=$User->findbyEmail($formData['email']);
            if($user->getIsActive()==false) {
                $commendData = new Command( $user, md5(uniqid(time())), new DateTime('+15 minutes'));
                $commendData->setResponder($this);
                $tokenExpire->execute($commendData);



                return $this->redirectToRoute('app_login');

            }



        }
        return $this->render('User/TokenExpire.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    public function UserTokenExpire(User $user)
    {
        $this->addFlash('success','Sprawdź maila: '.$user->getUsername());
    }
}