<?php


namespace App\Controller\Comment;


use App\Entity\Comment\Comment;
use App\Entity\Comment\Comment\UseCase\UnActivecomment;
use App\Entity\Comment\Comment\UseCase\UnActiveComment\Command;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UnActiveCommentController extends AbstractController
{
    /**
     * @param Comment $commentid
     * @param $reguest
     * @Route("admin/Comment/unActive/{commentid}", name="UnActiveComment", methods={"GET", "POST"})
     */
    public function main(Comment $commentid,UnActivecomment $unActivecomment)
    {
        $unActivecomment->execute(new Command($commentid));
        return $this->redirectToRoute('Post_Show',['PostId'=>$commentid->getPost()->getId()]);

    }
}