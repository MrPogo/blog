<?php


namespace App\Controller\Post;


use App\Adapter\Post\ReadModel\PostsQuery;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PostsControler extends AbstractController
{
    private $entityManager;
    public function __construct( EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction( PostsQuery $postsQuery)
    {
        return $this->render('Post\HomePage.html.twig',[
            'posts' => $postsQuery->getAllActiv(1),

        ]);
    }
}