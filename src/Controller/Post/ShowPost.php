<?php


namespace App\Controller\Post;


use App\Adapter\Comment\Comment;
use App\Adapter\Comment\ReadModel\CommentQuery;
use App\Adapter\Like\Like;
use App\Adapter\Like\ReadModel\LikeQuery;
use App\Adapter\Post\Posts;
use App\Adapter\Post\ReadModel\PostsQuery;
use App\Adapter\User\Users;
use App\Entity\Comment\Comment\UseCase\CreateComment;
use App\Entity\Comment\Comment\UseCase\CreateComment\Command;
use App\Entity\Like\Like\UseCase\CreateLike;
use App\Entity\Like\LikeInterface;
use App\Entity\Post\Post;
use App\Entity\Post\Posts\UseCase\UnActivePost;
use App\Entity\User\User;
use App\Form\Comment\AddComment;
use App\Form\Like\LikeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;


class ShowPost extends AbstractController
{
    private $entityManager;
    public function __construct( EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * @param int $PostId
     * @param Request $request
     * @return Response
     * @Route("/post/show/{PostId}", name="Post_Show", methods={"GET", "POST"})
     */
    public function postshow(Post $PostId,PostsQuery $postsQuery,Request $request,Users $User,Posts $posts,CreateComment $createComment,CommentQuery $commentQuery,CreateLike $createLike,LikeQuery $likeQuery,Like $likes)
    {
        /** @var User $user */
        $user=$this->getUser();
        $form=$this->createForm(AddComment::class);
        $form->handleRequest($request);
        $formLike=$this->createForm(LikeType::class);
        $formLike->handleRequest($request);
        $UnActiveForm=$this->createForm(LikeType::class);
        $UnActiveForm->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $formData = $form->getData();

            $commendData=new Command($formData['Comment'],$PostId,$user);
            $createComment->execute($commendData);

            return $this->redirectToRoute('Post_Show',['PostId'=>$PostId->getId()]);

        }
        if($formLike->isSubmitted())
        {

            $commendData=new CreateLike\Command($user,$PostId);
            $createLike->execute($commendData);
            return $this->redirectToRoute('Post_Show',['PostId'=>$PostId->getId()]);

        }



        return $this->render('Post/PostShow.html.twig',[
            'posts' => $postsQuery->getByIdActiv(1,$PostId->getId()),
            'formComment' => $form->createView(),
            'Comments'=>$commentQuery->getAllByIdPost($PostId->getId()),
            'formLike'=>$formLike->createView(),
            'count'=>count($likeQuery->findLikeInPost($PostId->getId())),
            'unlike'=>$likes->findLikeUserInPost($PostId->getId(),$user->getId()),
            'UnActiveForm'=>$UnActiveForm->createView(),
        ]);

    }




}