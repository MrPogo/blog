<?php


namespace App\Controller\Post;



use App\Adapter\Core\Transaction;
use App\Adapter\Post\Posts;
use App\Entity\Post\Post;
use App\Entity\Post\Posts\UseCase\CreatePost\Command;
use App\Form\Post\PostAddType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreatePost extends AbstractController
{


    /**
     * @param Request $request
     * @return Response
     * @Route("/admin/post/add", name="post_add")
     */
    public function PostCreate(Request $request,\App\Entity\Post\Posts\UseCase\CreatePost $post): Response
    {
        if ($this->getUser()->getRoles()!=["ROLE_ADMIN"]) {
            return $this->redirectToRoute('homepage');
        }

        $form=$this->createForm(PostAddType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()) {
            $formData = $form->getData();
            $commendData= new Command($formData['Title'],$formData['Message']);
            $post->execute($commendData);
        }
        return $this->render('Post/PostAdd.html.twig', [
            'form' => $form->createView(),
        ]);


    }



}