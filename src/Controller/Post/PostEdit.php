<?php


namespace App\Controller\Post;



use App\Adapter\Post\Posts;
use App\Entity\Post\Post;
use App\Entity\Post\Posts\UseCase\EditPost;
use App\Entity\Post\Posts\UseCase\EditPost\Command;
use App\Form\Post\PostAddType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostEdit extends AbstractController
{
    /**
     * @param Post $idPost
     * @param Posts $posts
     * @param $reguest
     * @Route("admin/Post/edit/{idPost}", name="Post_edit", methods={"GET", "POST"})
     */
    public function main(Post $idPost, Posts $posts,Request $reguest,EditPost $editPost)
    {
        $form=$this->createForm(PostAddType::class,$idPost);
        $form->handleRequest($reguest);
        if($form->isSubmitted()&&$form->isValid()) {
            $formData = $form->getData();

            $commendData= new Command($idPost,$formData->getTitle(),$formData->getMessage());
            $editPost->execute($commendData);

            return $this->redirectToRoute('Post_Show',['PostId'=>$idPost->getId()]);

        }
        return $this->render('Post/PostAdd.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}