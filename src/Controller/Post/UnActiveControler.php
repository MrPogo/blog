<?php


namespace App\Controller\Post;


use App\Adapter\Post\Posts;
use App\Entity\Post\Post;
use App\Entity\Post\Posts\UseCase\UnActivePost;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UnActiveControler extends AbstractController
{
    /**
     * @param Post $idPost
     * @param $reguest
     * @Route("admin/Post/unActivePost/{idPost}", name="UnActivePost", methods={"GET", "POST"})
     */
    public function main(Post $idPost,UnActivePost $unActivePost)
    {
        $commendData=new \App\Entity\Post\Posts\UseCase\UnActivePost\Command($idPost,false);
        $unActivePost->execute($commendData);
        return $this->redirectToRoute('homepage');

    }
}