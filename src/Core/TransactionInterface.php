<?php


namespace App\Core;


interface TransactionInterface
{
    public function begin();
    public function commit();
    public function rollBack();
}