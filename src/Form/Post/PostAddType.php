<?php


namespace App\Form\Post;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class PostAddType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Title',TextType::class, [
                'attr'=>[
                    'class'=>'form-control'
                ],
                'label'=>'Title'
            ])
            ->add('Message',TextareaType::class, [
                'attr'=>[
                    'class'=>'form-control'
                ],
                'label'=>'Post'
            ]);
    }

}