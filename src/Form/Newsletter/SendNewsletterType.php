<?php


namespace App\Form\Newsletter;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class SendNewsletterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Title')
            ->add('Message',TextareaType::class, [
                'attr'=>[
                'class'=>'form-control'
                ],
                'label'=>'Message'
            ]);
    }


}