<?php


namespace App\Form\Comment;


use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class AddComment extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Comment',TextareaType::class, [
                'attr'=>[
                    'class'=>'form-control'
                ],
                'label'=>'Comment'
            ]);
    }

}